def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if not all([type(side) in (int, float) for side in  (a,b,c)]):
        return False
    if (a+b) > c and (b+c) > a and (a+c) > b:
        return True
    else:
        return False
